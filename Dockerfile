FROM python:3.7-alpine
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . /app 

WORKDIR /app
# Lancer les tests lors du build de l'image.
RUN python3 -m "unittest" -v