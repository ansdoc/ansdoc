#!/usr/bin/env python
# -*- coding: utf-8 -*-

from optparse import OptionParser
from autodoc.documenter.ansibleproject import AnsibleProject
from autodoc.documenter.exporter import Exporter
from os import path
import glob

def main():
  parser = OptionParser()
  parser.add_option("-d", "--directory", help="Dossier à documenter - extension autorisée : .yml")
  parser.add_option("-t", "--template", help="Dossier contenant les fichiers de modèle")
  parser.add_option("-o", "--output", help="Dossier de sortie de la documentation")

  options, args = parser.parse_args()
  if not options.output:
    raise Exception("Aucun dossier de sortie sélectionné. (-o)")

  if not options.template:
    options.template = path.dirname(__file__) + '/../../../templates'

  files = glob.glob('host_vars/**/*', recursive=True)
  files += glob.glob('group_vars/**/*', recursive=True)

  project = AnsibleProject.parse(files)
  exporter = Exporter(options.template, project)
  exporter.markdown(options.output)
  exporter.save('index.md')

if __name__ == "__main__":
  main()