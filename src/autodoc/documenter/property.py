import re

class Property:
  def __init__(self, line):
    """
    A property is related to a declaration. It is a
    key: value comment line that creates documentation
    columns.
    """
    self.line = line.strip()
    if not self.validate_correctness(self.line):
      print("Mauvaise propriété: {}".format(line))

    self.key = self.getkey(self.line)
    self.value = self.getvalue(self.line)

  def validate_correctness(self, line: str) -> bool:
    """return true|false if matchs regex (# key: value)"""
    return bool(re.match(r"^#\s\w.*:\s.*", line))

  def get_key(self):
    """Getter for self.key"""
    return self.key

  def get_value(self):
    """Getter for self.value"""
    return self.value

  def getkey(self, line: str) -> str:
    """return string of line key (# key: value -> key)"""
    return line.split(":")[0].split("# ")[-1]

  def getvalue(self, line: str) -> str:
    """return string of line value (# key: value -> value)"""
    els = line.split(": ")
    return " ".join(els[1:])

