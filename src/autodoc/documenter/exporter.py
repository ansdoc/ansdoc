from autodoc.documenter.ansibleproject import AnsibleProject
from mako.template import Template
from datetime import datetime
import os.path
from os import mkdir

class Exporter:
  def __init__(self, template_dir: str, app: AnsibleProject):
    self.template_dir = template_dir
    self.binds = app

  def markdown(self, output_dir: str) -> bool:
    """
    Exports abstract tree from self.binds to Jinja2
    template that generates markdown files in self.template_dir.
    @todo: replace "replace()" method call by something cleaner.
    """
    self.output_dir = output_dir
    template_content = ""

    with open("{}/single.md.mako".format(self.template_dir), "r", encoding="utf-8") as f:
      template_content = f.read()

    template = Template(template_content)
    result = template.render(
      binds=self.binds,
      gen_date=datetime.now()
      ).replace("  ", "")

    self.result = result

  def ensure_directory(self) -> bool:
    """
    Ensure output directory exists
    """
    if not os.path.exists(self.output_dir):
      mkdir(self.output_dir)

  def save(self, file_name: str) -> bool:
    """
    Creates a file and stores the generation result.
    """
    self.ensure_directory()
    with open("{}/{}".format(self.output_dir, file_name), "w+", encoding="utf-8") as f:
      f.write(self.result)
