from os import path
from autodoc.documenter.annotation import Annotation

class AnsibleFile:
    def __init__(self, path):
        self.annotations = []
        self.path = path
        self.clean_path = self.path.replace('/', '-')

    def get_filename(self):
        return path.basename(self.path)

    def get_path(self):
        return self.path

    def read(self):
        with open(self.path, 'r', encoding="utf-8") as f:
            self.content = f.read()
        self.parse_annotations()

    def get_annotations(self):
        return self.annotations

    def parse_annotations(self):
        """reads all lines in file and creates abstract tree"""
        block = False
        current_block = []
        all_blocks = []
        lines = [x.strip() for x in self.content.split("\n")]

        for line in lines:
            if line.startswith("#"):
                block = True
                current_block.append(line)
            else:
                block = False
                if len(current_block) != 0:
                    all_blocks.append(current_block)
                current_block = []
        for block in all_blocks:
            if "@" in block[0]:
                annotation = Annotation(block)
                self.annotations.append(annotation)
