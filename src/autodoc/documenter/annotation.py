from autodoc.documenter.declaration import *
from autodoc.documenter.property import *

class Annotation:
  """
  An annotation is a block of comment lines about
  a single subject, composed of a declaration
  and properties.
  """
  def __init__(self, block):
    self.declaration = Declaration(block[0])
    self.properties: dict = {}

    for prop in block[1:]:
      property = Property(prop)
      self.properties[property.get_key()] = property.get_value()

  def get_declaration(self):
    """Getter for self.declaration"""
    return self.declaration

  def get_properties(self):
    """Getter for self.properties"""
    return self.properties
