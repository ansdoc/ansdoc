import re

class Declaration:
  """
  A declaration targets the first annotation block line.
  It begins with @ as identifier and refers to properties.
  """
  def __init__(self, line: str):
    self.line = line.strip()
    if not self.validate_correctness(self.line):
      print("Mauvaise déclaration: {}".format(line))

    self.kind = self.getkind(self.line)
    self.value = self.getvalue(self.line)

  def validate_correctness(self, line: str) -> bool:
    """return true|false if matchs regex (# @x: y)"""
    return bool(re.match(r"#\s@\w.*:\s.*", line))

  def getkind(self, line: str) -> str:
    """return string of line kind (# @key: y -> key)"""
    return line.split("# @")[-1].split(":")[0]

  def getvalue(self, line: str) -> str:
    """return string of line value (# @key: y -> y)"""
    return line.split(": ")[-1]

