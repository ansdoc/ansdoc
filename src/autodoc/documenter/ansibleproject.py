from typing import Type
from autodoc.documenter.ansiblefile import AnsibleFile

class AnsibleProject:
  def __init__(self):
    self.files: list[AnsibleFile] = []

  def get_files(self) -> list[AnsibleFile]:
        return self.files

  def get_annotations_by_kind(self, kind: str):
    annotations = []
    for f in self.files:
      annotations += list(filter(lambda x: x.declaration.kind == kind, f.annotations))
    return annotations

  @staticmethod
  def parse(files: list[str]) -> 'AnsibleProject':
    project = AnsibleProject()
    for file in files:
      ansible_file = AnsibleFile(file)
      ansible_file.read()
      ansible_file.parse_annotations()
      project.files.append(ansible_file)
    return project
