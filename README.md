@TODO: traduire en anglais

> **Ce projet est en cours de développement et n'a pas encore livré sa premiere version.**

> Ce README est principalement un README de developpement.

# AnsDoc

**AnsDoc** generates usefull documentations for ansible roles, playbooks and inventories.

## Pourquoi ?

### Une histoire d'API

> Ansible est un outils de gestion de configuration d'application et d''infrastructure IT.

En tant que developpeur, vous utilisez au quotidien des API's. L'interface offrant a l'utilisateur les fonctionnalites de l'API est generalement materialise sous la forme d'un ensemble de methodes a appeler. Ces methodes sont documentees et la documentation est regulierement a jour, permettant a l'utilisateur de suivre l'evolution de l'interface, connaitre les nouvelles fonctionnalites mais aussi les methodes ou les points d'acces qui seront supprimes dans une future version majeure.

Ansible est un outils DevOps. Les inventaires, roles et collections que les developpeurs creent a destination d'autres utilisateurs servent d'API. A la difference d'une API classique, les methodes permettant d'interagir avec l'API se font par le biais de variables et non de fonctions. Ces variables, correctement renseignees, permettent d'obtenir l'etat de configuration de l'infrastructure ciblée. Il devient donc important de documenter ces variables et de tenir a jour cette documentation au fil des évolutions du projet.

Nous faisons le choix d'une documentation "in-code" pour auto-documenter les variables et tags ansible. Ce choix nous parait le plus pertinent pour generer une documentation fiable et evolutive tout en minimisant les taches du developpeur pour entretenir la documentation. Ce choix est bien sur discutable et la retro-documentation n'est pas exempte de defauts.

### Les problématiques

- La lisibilite des fichiers peut etre impactee par l'ajout d'annotations
- Il y a un soucis de savoir quand une variable a ete ajoutee ou supprimee
- Un commentaire dit sa verite, cela sous entend que si un developpeur supprime une variable mais oublie de supprimer l'annotation liee a cette variable, le commentaire reste vrai et induit inevitablement l'utilisateur en erreur.
- Le cas des variables depreciees est important. Comment offrir le maximum d'automatisation pour detecter et generer la doc relatives a ces variables ?

Nous tenterons, au fil des evolutions d'ansdoc, de resoudre de solutionner simplement ces problemes.

## Installation

TBD (pas encore de release, voir le chapitre `developpement`)

## Usage

```bash
  ansdoc --help
```

Actuellement, ansdoc ne gere que la generation d'une documentation d'inventaire.

Pour generer la documentation d'un inventaire :

```bash
cd ansible_example_project
# l'option -o designe le dossier dans lequel la doc est genere.
ansdoc -o res/
```

Pour generer une documentation a partir d'un inventaire, avec vos propres templates:
```bash
cd ansible_example_project
# l option -t designe le dossier ou sont presents les templates
ansdoc -t templates/ -o res/
```

> l'option permettant d'integrer vos templates est en phase de developpement et peu fonctionnelle.


### Documentation syntax


```yaml
# @var: linux_local_users
# @tags: public deprecated v1.32
# @description:
# a list of local users.
# A local user is composed by a name and a password. 
# Password must be encrypted.
# @example:
# linux_local_users:
#   - name: "toto"
#     password: "$encryptedpassword"
linux_local_users:
  - name: "admin"
    password: "$6$mysecretsalt$rbeo5PlqGbPuwqeqwe8vwcXyXH8.dLd8q2t0KoGhUz81kuMtU2v8MPhZXaV8rxYYPEV35iQ2TE.31I/cJmTK1lO2zWk."
  - name: "root"
    password: "$6$mysecretsalt$rbrewrewrewrewreo5PlqGbPu8vwcXyXH8.dLd8q2t0KoGhUz81kuMtU2v8MPhZXaV8rxYYPEV35iQ2TE.31I/cJmTK1lO2zWk."
```

## Documentation

```bash
$ pydoc -b
Server> b
```

## Development

Les Pull Requests sont les bienvenues. Prenez une issue et aidez nous a developper cette solution !

Pour proposer une amelioration ou faire remonter un fait technique, veillez a verifier qu'elle n'existe pas dans la liste des issues.

Vous voulez aider mais ne savez pas par ou commencer ? Consultez le [kaban de developpement](https://gitlab.com/gigistone/ansdoc/-/boards/2514361)! Je prend soin de decouper le plus possible les stories avec les autres constributeurs. De petites taches sont realisables independamment du niveau de chacun.

> Une PR de feature ne sera acceptee que si les tests liees a la feature ont ete developpes et sont operationnels 

### Installation

Pour installer et travailler sur AnsDoc localement (avec pip dans un environnement virtuel):

```bash
git clone https://gitlab.com/gigistone/ansdoc.git
cd ansdoc
python -m venv env
source env/bin/activate
pip install -e .
```

### Execution des tests

```bash
# a la racine du projet
python -m unittest
```

Une config vscode est presente dans le projet et integere les tests.
Vous etes sous pycharm ? Une config a appliquer ? Faites la conf et proposez son integration dans le depot.
