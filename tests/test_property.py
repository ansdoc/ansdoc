from autodoc.documenter.property import Property
from unittest.mock import patch
import unittest
from io import StringIO 

class TestProperty(unittest.TestCase):
    def setUp(self):
        self.inst = Property("# key: value")

    def test_prints_a_str_bad_property(self):
        with patch('sys.stdout', new = StringIO()) as fake_out: 
            Property("# a bad property")
            self.assertIn("Mauvaise propriété", fake_out.getvalue())

    def test_get_key_returns_key(self):
        self.assertIsInstance(self.inst.key, str)
        self.assertEqual(self.inst.key, "key")

    def test_get_value_returns_value(self):
        self.assertIsInstance(self.inst.value, str)
        self.assertEqual(self.inst.value, "value")

    def test_validate_correctness_return_false_on_bad_arg(self):
        prop = Property("# key: value").validate_correctness("# not_ok")
        self.assertIsInstance(prop, bool)
        self.assertEqual(prop, False)