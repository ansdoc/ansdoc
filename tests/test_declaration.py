from autodoc.documenter.declaration import Declaration
from unittest.mock import patch
import unittest
from io import StringIO 

class TestDeclaration(unittest.TestCase):
    def setUp(self):
        self.inst = Declaration("# @var: a_correct_declaration")

    def test_prints_a_str_bad_declaration(self):
        with patch('sys.stdout', new = StringIO()) as fake_out: 
            Declaration("# a bad declaration")
            self.assertIn("Mauvaise déclaration", fake_out.getvalue())

    def test_accept_correct_declaration_line(self):
        self.assertIsInstance(self.inst, Declaration)

    def test_getkind_returns_kind(self):
        self.assertIsInstance(self.inst.kind, str)
        self.assertEqual(self.inst.kind, "var")

    def test_getvalue_returns_value(self):
        self.assertIsInstance(self.inst.value, str)
        self.assertEqual(self.inst.value, "a_correct_declaration")

    def test_validate_correctness_return_false_on_bad_arg(self):
        self.assertIsInstance(Declaration("# @var: good").validate_correctness("nop"), bool)