from autodoc.documenter.ansibleproject import AnsibleProject
import unittest

class TestAnsibleProject(unittest.TestCase):
    def setUp(self):
        self.inst = AnsibleProject()

    def tearDown(self):
        pass

    def test_instance_is_created(self):
        self.assertIsInstance(self.inst, AnsibleProject)
