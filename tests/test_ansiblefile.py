from autodoc.documenter.ansiblefile import AnsibleFile
import os
import unittest
import errno

class TestAnsibleFile(unittest.TestCase):
    def setUp(self):
        content = """
            # @var: test
            # key: value
            test: value
        """

        self.filename = "goodfile.yml"
        self.path = "goodpath/" + self.filename

        # if not os.path.exists(os.path.dirname(self.path)):
        #     try:
        #         os.makedirs(os.path.dirname(self.path))
        #     except OSError as exc: # Guard against race condition
        #         if exc.errno != errno.EEXIST:
        #             raise

        # with open(self.path, "w+", encoding="utf-8") as f:
        #     f.write("\n".join(example_content))

        self.inst = AnsibleFile(self.path)
        self.inst.content = content

    def tearDown(self):
        if os.path.exists(self.path):
            os.remove(self.path)

    def test_instance_is_created(self):
        self.assertIsInstance(self.inst, AnsibleFile)

    def test_get_path_returns_path(self):
        self.assertEqual(self.inst.path, self.path)

    def test_get_annotations_returns_empty_array(self):
        self.assertEqual(self.inst.annotations, [])

    def test_get_annotations_should_return_annotations_after_load(self):
        self.inst.parse_annotations()
        self.assertEqual(1, len(self.inst.annotations))

    def test_get_filename_should_return_filename(self):
        self.assertEqual(self.filename, self.inst.get_filename())

