from autodoc.documenter.annotation import Annotation
from autodoc.documenter.declaration import Declaration

import unittest

class TestAnnotation(unittest.TestCase):
    def setUp(self):
        self.fake_annotation = [
            "# @var: fake_annotation",
            "# test_file: test_annotation.py",
            "# test_class: TestAnnotation",
            "# awaited_result: ok"
        ]

        self.inst = Annotation(self.fake_annotation)

    def test_get_declaration_returns_declaration(self):
        self.assertIsInstance(self.inst.declaration, Declaration)

    def test_get_properties_returns_array_of_properties(self):
        self.assertIsInstance(self.inst.properties, dict)
        for key, value in self.inst.properties.items():
            self.assertIsInstance(key, str)
            self.assertIsInstance(value, str)

# suite = unittest.TestSuite()
# suite.addTests([TestAnnotation(), TestBadAnnotation()])