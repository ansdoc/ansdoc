<% variables = binds.get_annotations_by_kind("var") %>
<% variables_amount = len(variables) %>

# SmartNOC Inventory

${"## Variables d'inventaire"}

% for annotation in variables:
  - <a href="#var-${annotation.declaration.value}">${annotation.declaration.value}</a>
% endfor

% for annotation in variables:
  ${'###'} ${annotation.declaration.value}<span id="var-${annotation.declaration.value}"></span> \
  ${'`internal`' if annotation.properties.get('visibility') != 'public' else ''}

  ${'> ' + (annotation.properties.get('description') or 'N/A')}


  ```yaml
  # this is an example
  toto: 42
  ```

  ${'#### Meta-data'}

  |Propriété|Valeur|
  |---------|------|
  % for key, value in annotation.properties.items():
    |${key}|${value}|
  % endfor
% endfor

