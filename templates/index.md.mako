<% import glob %>
<% files = glob.glob("{}/**/*.md".format(output_dir), recursive=True) %>
# Index

|Fichier|Date|
|-------|----|
|`${binds.path}`|${gen_date}|

% for file in files:
  % if "/" in file:
    * [${file}](${"".join(file.split("/")[1:])})
  % else:
    * [${file}](${file})
  % endif
% endfor

